package pl.sebhaw.counters.moka7client;

import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import pl.sebhaw.counters.entity.Machine;
import pl.sebhaw.counters.service.MachineServiceImpl;

@Component
@Slf4j
public class ScheduledMoka7Client {

	public final MachineServiceImpl machineService;

	public ScheduledMoka7Client(MachineServiceImpl machineService) {
		this.machineService = machineService;
	}

//	@Scheduled(cron = "*/2 * * * * *")
	@Transactional(rollbackOn = Exception.class)
	public void scheduledUpdateMachineCounter() {

		Runnable runnable1 = () -> {
			Machine machine = new Machine();
			try {
				List<Machine> machines = machineService.getMachine(1);
				machine = machines.iterator().next();
			} catch(NullPointerException e) {
				e.printStackTrace();
			}
			Moka7Client.schedulev2("192.168.3.3", 2020123, machine, 1);
		};

		Runnable runnable2 = () -> {
			Machine machine = new Machine();
			try {
				List<Machine> machines = machineService.getMachine(2);
				machine = machines.iterator().next();
			} catch(NullPointerException e) {
				e.printStackTrace();
			}
			Moka7Client.schedulev2("192.168.3.4", 11111, machine, 2);
		};

		Runnable runnable3 = () -> {
			Machine machine = new Machine();
			try {
				List<Machine> machines = machineService.getMachine(3);
				machine = machines.iterator().next();
			} catch(NullPointerException e) {
				e.printStackTrace();
			}
			Moka7Client.schedulev2("192.168.3.5", 202221123, machine, 3);
		};

		Thread thread1 = new Thread(runnable1);
		thread1.start();
		Thread thread2 = new Thread(runnable2);
		thread2.start();
		Thread thread3 = new Thread(runnable3);
		thread3.start();
	}
	
	@Scheduled(cron = "*/1 * * * * *")
	@Transactional(rollbackOn = Exception.class)
	public void scheduledUpdateMachineCounterSimulator() {
		
		Runnable runnable1 = () -> {
			try {
				List<Machine> machines = machineService.getMachine(1);
				Machine machine = machines.get(0);
				machine.setCounter(machine.getCounter() + new Random().nextInt(8-2)+2);

				machineService.updateMachine(1, machine);
			} catch(NullPointerException e) {
				e.printStackTrace();
			}
		};
		
		Runnable runnable2 = () -> {
			try {
				List<Machine> machines = machineService.getMachine(2);
				Machine machine = machines.get(0);
				machine.setCounter(machine.getCounter() + new Random().nextInt(10-2)+2);

				machineService.updateMachine(2, machine);
			} catch(NullPointerException e) {
				e.printStackTrace();
			}
		};
		
		Runnable runnable3 = () -> {
			try {
				List<Machine> machines = machineService.getMachine(3);
				Machine machine = machines.get(0);
				machine.setCounter(machine.getCounter() + new Random().nextInt(12-2)+2);
				machineService.updateMachine(3, machine);
			} catch(NullPointerException e) {
				e.printStackTrace();
			}
		};
		
		Thread thread1 = new Thread(runnable1);
		thread1.start();
		Thread thread2 = new Thread(runnable2);
		thread2.start();
		Thread thread3 = new Thread(runnable3);
		thread3.start();
	}
}
