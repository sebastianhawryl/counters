# CountersPLC REST API

### CountersPLC allows to read machine parameters using the plc driver and share them via REST API.
### It allows to read parameters such as: sheet counter, air flow meter, current intensity or vibration sensor.

### Detailed application design available at: [a link](https://sebastianhawryl.kylos.pl/2020/12/29/countersplc-project-eng/)

### API url: [a link](https://countersplc.herokuapp.com)

### Endpoints
* https://countersplc.herokuapp.com/authenticate - POST mapping, authenticate user creditials and generate JWT Token. Creditials(JSON): {"username":"user", "password":"user","role":"ROLE_USER"}
* https://countersplc.herokuapp.com/machine - GET mapping, view of all machines. Required JWT Token
* https://countersplc.herokuapp.com/machine/{id} - GET mapping, view of single machine. Id value between 1-3. Required JWT Token


### Stack of technologies:
* Spring: Boot, Framework(MVC, DI), Security
* H2 Database, JPA(JpaRepository)
* Tests: JUnit 5, Mockito 2

### Stack of tools:
* Eclipse IDE
* Postman
* Git (Bitbucket, SourceTree)
* Jenkins
* Docker
* Moka7 for connecting with PLC driver
