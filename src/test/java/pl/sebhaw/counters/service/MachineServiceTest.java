package pl.sebhaw.counters.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.annotation.Rollback;

import pl.sebhaw.counters.entity.Machine;
import pl.sebhaw.counters.repository.MachineRepository;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
@Transactional
public class MachineServiceTest {
	
	@InjectMocks
	private MachineServiceImpl machineService;
	
	@Mock
	private MachineRepository machineRepository;
	
	@Mock
	private Machine machine;
	
	@BeforeEach
	void init() {
		machine = Machine.builder()
				.id(1)
				.name("machineTest")
				.counter(123123)
				.build();
	}
	
	@Test
	@Rollback(true)
	void shouldReturnEmptyCollection() {
		List<Machine> machines = new ArrayList<Machine>();
		
		Mockito.when(machineRepository.findAll()).thenReturn(machines);
		List<Machine> expectedMachines = machineService.getAllMachines();
		
		assertEquals(machines, expectedMachines);
	}
	
	@Test
	@Rollback(true)
	void shouldSaveMachineSuccessfully() {
		
		Mockito.when(machineRepository.save(Mockito.any(Machine.class))).thenReturn(machine);
		
		machineService.updateMachine(machine.getId(), machine);
		
		Mockito.verify(machineRepository, Mockito.times(1)).save(Mockito.any(Machine.class));
		Mockito.verifyNoMoreInteractions(machineRepository);
	}
	
	@Test
	@Rollback(true)
	void shouldDeleteMachineSuccessfully() {
		
		Mockito.doNothing().when(machineRepository).deleteById(Mockito.anyInt());
		
		machineRepository.deleteById(machine.getId());
		
		Mockito.verify(machineRepository, Mockito.times(1)).deleteById(machine.getId());
		Mockito.verifyNoMoreInteractions(machineRepository);
	}
	
	@Test
	@Rollback(true)
	void shouldReturnCollectionOfOneMachine() {
		
		List<Machine> machines = new ArrayList<Machine>();
		machines.add(machine);
		Mockito.when(machineRepository.findAll()).thenReturn(machines);
		
		List<Machine> expectedMachines = machineService.getAllMachines();
		
		assertEquals(machines, expectedMachines);
	}
	
	@Test
	@Rollback(true)
	void shouldReturnMachineWithCorrectId() {
		
		List<Machine> machines = new ArrayList<Machine>();
		machines.add(machine);
		
		Mockito.when(machineRepository.findAllById(Collections.singleton(ArgumentMatchers.any()))).thenReturn(machines);
		
		List<Machine> result = machineService.getMachine(machine.getId());
		
		for(Machine tempMachine: result) {
			assertAll(
					() -> assertEquals(1, tempMachine.getId()),
					() -> assertEquals("machineTest", tempMachine.getName()),
					() -> assertEquals(123123, tempMachine.getCounter())
					);
		}
	}
	
	@Test
	@Rollback(true)
	void shouldReturnNullValueOfMachineWithNonExistingId() {
		
		Mockito.when(machineRepository.findAllById(Collections.singleton(ArgumentMatchers.any()))).thenReturn(null);
		
		List<Machine> machines = machineService.getMachine(Mockito.anyInt());
		
		assertNull(machines);
	}

}
