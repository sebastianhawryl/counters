package pl.sebhaw.counters.contracts;

import org.junit.jupiter.api.BeforeEach;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import pl.sebhaw.counters.jwt.JwtAuthenticationController;

public abstract class BaseClass {
	
	@BeforeEach
	public void setup() {
		RestAssuredMockMvc.standaloneSetup(new JwtAuthenticationController());
	}
}
