package pl.sebhaw.counters.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public interface IReturnPrincipal {
	
		default String getPrincipal() {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if(principal instanceof UserDetails) {
				return new String(((UserDetails)principal).getUsername());
			} else {
				return new String(principal.toString());
			}
	}

}
