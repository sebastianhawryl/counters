package pl.sebhaw.counters.jwt;

import javax.transaction.Transactional;

import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.OrderedJSONObject;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.thymeleaf.spring5.expression.Mvc;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class JwtGenerateTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@InjectMocks
	private JwtAuthenticationController jwtAuthenticationController;

	@Test
	public void existingUserCreditialsShouldGenerateTokenAndAuthentication() throws Exception {
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/authenticate")
				.content(returnCorrectJSONObject().toString()))
		.andExpect(status().isOk()).andReturn();
		
		String response = result.getResponse().getContentAsString();
		String token = "Bearer "+response;
		
		mockMvc.perform(get("/machine")
				.header("Authorization", token))
		.andExpect(status().isOk());
		
		
	}
	
	private static OrderedJSONObject returnCorrectJSONObject() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, JSONException {
		String username="user";
		String password="$2y$12$RLizx4CyzeE0Tp4r0I9rBurYkJJfoOm9iJqCrKaQW/W5XJQjt8msC";
		String role="ROLE_USER";
		
		OrderedJSONObject body = new OrderedJSONObject();
		body.put("username", username);
		body.put("password", password);
		body.put("role", role);
		System.out.println("Body: "+body);
		return body;
	}
}
