package pl.sebhaw.counters.architecture;
import org.junit.jupiter.api.BeforeAll;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;

public abstract class ArchitectureTest {
	
	static final String JWT_LAYER_PACKAGES = "pl.sebhaw.counters.jwt..";
	static final String SERVICE_LAYER_PACKAGES = "pl.sebhaw.counters.service..";
	static final String CONTROLLER_LAYER_PACKAGES = "pl.sebhaw.counters.controller..";
	static final String REPOSITORY_LAYER_PACKAGES = "pl.sebhaw.counters.repository..";
	static final String ENTITY_LAYER_PACKAGES = "pl.sebhaw.counters.entity..";
	static final String MOKA7_LAYER_PACKAGES = "pl.sebhaw.counters.moka7..";
	static final String MOKA7CLIENT_LAYER_PACKAGES = "pl.sebhaw.counters.moka7client..";
	static final String APPLICATION_LAYER_PACKAGES = "pl.sebhaw.counters..";
	static final String CONFIGURATION_LAYER_PACKAGES = "pl.sebhaw.counters.config..";
	
	static JavaClasses classes;
	
	@BeforeAll
	public static void setUp() {
		classes = new ClassFileImporter()
				.withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
				.withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_ARCHIVES)
				.importPackages("pl.sebhaw.counters");
	}
}
