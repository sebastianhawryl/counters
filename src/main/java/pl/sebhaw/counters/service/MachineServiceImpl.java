package pl.sebhaw.counters.service;

import org.springframework.stereotype.Service;
import pl.sebhaw.counters.entity.Machine;
import pl.sebhaw.counters.repository.MachineRepository;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

@Service
public class MachineServiceImpl implements MachineService{

	private final MachineRepository machineRepository;

    public MachineServiceImpl(MachineRepository machineRepository) {
        this.machineRepository = machineRepository;
    }

    @Transactional(rollbackOn = Exception.class)
    @Override
    public List<Machine> getAllMachines() {
		return machineRepository.findAll();
	}

    @Transactional(rollbackOn = Exception.class)
    @Override
    public List<Machine> getMachine(int id) {
        return machineRepository.findAllById(Collections.singleton(id));
    }

    @Transactional(rollbackOn = Exception.class)
    @Override
    public void addMachine(Machine machine) {
        machineRepository.save(machine);
    }

    @Transactional(rollbackOn = Exception.class)
    @Override
    public void updateMachine(int id, Machine machine) {
        machineRepository.save(machine);
    }

    @Transactional(rollbackOn = Exception.class)
    @Override
    public void deleteMachine(int id) {
        machineRepository.deleteById(id);
    }
}