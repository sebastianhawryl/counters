package pl.sebhaw.counters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EntityScan("pl.sebhaw.counters.entity")
@EnableScheduling
public class CountersApplication {
	public static void main(String[] args) {
		SpringApplication.run(CountersApplication.class, args);
	}
}
