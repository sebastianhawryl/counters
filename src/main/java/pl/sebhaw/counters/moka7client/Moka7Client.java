package pl.sebhaw.counters.moka7client;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import pl.sebhaw.counters.entity.Machine;
import pl.sebhaw.counters.moka7.IntByRef;
import pl.sebhaw.counters.moka7.S7;
import pl.sebhaw.counters.moka7.S7BlockInfo;
import pl.sebhaw.counters.moka7.S7Client;
import pl.sebhaw.counters.moka7.S7CpInfo;
import pl.sebhaw.counters.moka7.S7CpuInfo;
import pl.sebhaw.counters.moka7.S7OrderCode;
import pl.sebhaw.counters.moka7.S7Protection;
import pl.sebhaw.counters.moka7.S7Szl;
import pl.sebhaw.counters.service.MachineServiceImpl;

public class Moka7Client {
	
	@Autowired
	public static MachineServiceImpl machineService;
	
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final boolean MakeAllTests = true;
   
    private static long Elapsed;
    private static byte[] Buffer = new byte[65536];
    private static final S7Client Client = new S7Client();
    private static int ok=0;
    private static int ko=0;    
    private static String IpAddress = "192.168.3.3";
    private static int Rack = 0;
    private static int Slot = 0;
    private static int DBSample = 11;
    private static int DataToMove;
    private static int CurrentStatus = S7.S7CpuStatusUnknown;
    
    public static void HexDump(byte[] Buffer, int Size)
    {
        int r=0;
        String Hex = "";
        
        for (int i=0; i<Size; i++)
        {
            int v = (Buffer[i] & 0x0FF);
            String hv = Integer.toHexString(v);     
            
            if (hv.length()==1)
                hv="0"+hv+" ";
            else
                hv=hv+" ";
            
            Hex=Hex+hv;
            
            r++;
            if (r==16)
            {
                System.out.print(Hex+" ");
                System.out.println(S7.GetPrintableStringAt(Buffer, i-15, 16));
                Hex="";
                r=0;
            }
        }
        int L=Hex.length();
        if (L>0)
        {
            while (Hex.length()<49)
                Hex=Hex+" ";
            System.out.print(Hex);
            System.out.println(S7.GetPrintableStringAt(Buffer, Size-r, r));                       
        }
        else
            System.out.println();
    }
    
    static void TestBegin(String FunctionName)
    {
        System.out.println();
        System.out.println("+================================================================");
        System.out.println("| "+FunctionName);
        System.out.println("+================================================================");
        Elapsed = System.currentTimeMillis();
    }
    
    static void TestEnd(int Result)
    {
    	if (Result!=0)
    	{
    		ko++;
    		Error(Result);
    	}
    	else
    		ok++;
    	System.out.println(" ution time "+(System.currentTimeMillis()-Elapsed)+" ms");
    }
       
    static void Error(int Code)
    {
        System.out.println(S7Client.ErrorText(Code));        
    }
    
    static void BlockInfo(int BlockType, int BlockNumber)
    {
        S7BlockInfo Block = new S7BlockInfo();
        TestBegin("GetAgBlockInfo()");       
        
        int Result = Client.GetAgBlockInfo(BlockType, BlockNumber, Block);
        if (Result==0)
        {
            System.out.println("Block Flags     : "+Integer.toBinaryString(Block.BlkFlags()));
            System.out.println("Block Number    : "+Block.BlkNumber());
            System.out.println("Block Languege  : "+Block.BlkLang());
            System.out.println("Load Size       : "+Block.LoadSize());
            System.out.println("SBB Length      : "+Block.SBBLength());
            System.out.println("Local Data      : "+Block.LocalData());
            System.out.println("MC7 Size        : "+Block.MC7Size());
            System.out.println("Author          : "+Block.Author());
            System.out.println("Family          : "+Block.Family());
            System.out.println("Header          : "+Block.Header());
            System.out.println("Version         : "+Block.Version());
            System.out.println("Checksum        : 0x"+Integer.toHexString(Block.Checksum()));
            SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy");
            System.out.println("Code Date       : "+ft.format(Block.CodeDate()));
            System.out.println("Interface Date  : "+ft.format(Block.IntfDate()));
        }
        TestEnd(Result);
    }
    
    public static boolean DBGet()
    {
        IntByRef SizeRead = new IntByRef(0);
    	TestBegin("DBGet()");
        int Result = Client.DBGet(DBSample, Buffer, SizeRead);
        TestEnd(Result);        
        if (Result==0)
        {
        	DataToMove = SizeRead.Value; // Stores DB size for next test
        	System.out.println("DB "+DBSample+" - Size read "+DataToMove+" bytes");
        	HexDump(Buffer, DataToMove);
        	return true;
        }        
        return false;        
    }
    
    public static void DBRead()
    {
    	TestBegin("ReadArea()");
    	int Result = Client.ReadArea(S7.S7AreaDB, DBSample, 0, DataToMove, Buffer);
        if (Result==0)
        {
        	System.out.println("DB "+DBSample+" succesfully read using size reported by DBGet()");
        }
        TestEnd(Result);
    }      
    
    public static void DBWrite()
    {
    	TestBegin("WriteArea()");
    	int Result = Client.WriteArea(S7.S7AreaDB, DBSample, 0, DataToMove, Buffer);
        if (Result==0)
        {
        	System.out.println("DB "+DBSample+" succesfully written using size reported by DBGet()");
        }
    	TestEnd(Result);
    }      
    
    public static void DBPlay()
    {
    	if (DBGet())
    	{
    		DBRead();
    		if (MakeAllTests)
    			DBWrite();
    	}
    }
        
    public static void Delay(int millisec)
    {
        try {
            Thread.sleep(millisec);
        }
        catch (InterruptedException e) {}                    
    }
    
    public static void ShowStatus()
    {
        IntByRef PlcStatus = new IntByRef(S7.S7CpuStatusUnknown);
        TestBegin("GetPlcStatus()");       
        int Result = Client.GetPlcStatus(PlcStatus);
        if (Result==0)
        {
            System.out.print("PLC Status : ");
            switch (PlcStatus.Value)
            {
                case S7.S7CpuStatusRun :
                    System.out.println("RUN");
                    break;
                case S7.S7CpuStatusStop :
                    System.out.println("STOP");
                    break;
                default :    
                    System.out.println("Unknown ("+PlcStatus.Value+")");
            }
        }
        CurrentStatus = PlcStatus.Value;
        TestEnd(Result);
    }

    public static void DoRun()
    {
        TestBegin("PlcHotStart()");
        int Result = Client.PlcHotStart();
        if (Result==0)
        	System.out.println("PLC Started");
        TestEnd(Result);    	
    }

    public static void DoStop()
    {
        TestBegin("PlcStop()");
        int Result = Client.PlcStop();
        if (Result==0)
        	System.out.println("PLC Stopped");
        TestEnd(Result);    	    	
    }
    
    public static void RunStop()
    {
        switch (CurrentStatus)
        {
        	case S7.S7CpuStatusRun :
        		DoStop();
        		Delay(1000);
        		DoRun();
        		break;
        	case S7.S7CpuStatusStop :	
        		DoRun();
        		Delay(1000);
        		DoStop();
        }       
    }
    
    public static void GetSysInfo()
    {
        int Result;
        TestBegin("GetOrderCode()");
        S7OrderCode OrderCode = new S7OrderCode();
        Result = Client.GetOrderCode(OrderCode);
        if (Result==0)
        {
            System.out.println("Order Code        : "+OrderCode.Code());
            System.out.println("Firmware version  : "+OrderCode.V1+"."+OrderCode.V2+"."+OrderCode.V3);
        }
        TestEnd(Result);
        
        TestBegin("GetCpuInfo()");
        S7CpuInfo CpuInfo = new S7CpuInfo();
        Result = Client.GetCpuInfo(CpuInfo);
        if (Result==0)
        {
            System.out.println("Module Type Name  : "+CpuInfo.ModuleTypeName());
            System.out.println("Serial Number     : "+CpuInfo.SerialNumber());
            System.out.println("AS Name           : "+CpuInfo.ASName());
            System.out.println("CopyRight         : "+CpuInfo.Copyright());
            System.out.println("Module Name       : "+CpuInfo.ModuleName());
        }
        TestEnd(Result);

        TestBegin("GetCpInfo()");
        S7CpInfo CpInfo = new S7CpInfo();
        Result = Client.GetCpInfo(CpInfo);
        if (Result==0)
        {
            System.out.println("Max PDU Length    : "+CpInfo.MaxPduLength);
            System.out.println("Max connections   : "+CpInfo.MaxConnections);
            System.out.println("Max MPI rate (bps): "+CpInfo.MaxMpiRate);
            System.out.println("Max Bus rate (bps): "+CpInfo.MaxBusRate);
        }
        TestEnd(Result);
    }
    
    public static void GetDateAndTime()
    {
        Date PlcDateTime = new Date();
        TestBegin("GetPlcDateTime()");
        int Result = Client.GetPlcDateTime(PlcDateTime);
        if (Result==0)
            System.out.println("CPU Date/Time : "+PlcDateTime);
        TestEnd(Result);
    }    

    public static void SyncDateAndTime()
    {
        TestBegin("SetPlcSystemDateTime()");
        int Result = Client.SetPlcSystemDateTime();
        TestEnd(Result);
    }    
    
    public static void ReadSzl()
    {
        S7Szl SZL = new S7Szl(1024);
        TestBegin("ReadSZL() - ID : 0x0011, IDX : 0x0000");
        int Result = Client.ReadSZL(0x0011, 0x0000, SZL);
        if (Result==0)
        {
            System.out.println("LENTHDR : "+SZL.LENTHDR);
            System.out.println("N_DR    : "+SZL.N_DR);
            System.out.println("Size    : "+SZL.DataSize);                
            HexDump(SZL.Data,SZL.DataSize); 
        }
        TestEnd(Result);
    }
    
    public static void GetProtectionScheme()
    {
        S7Protection Protection = new S7Protection();
        TestBegin("GetProtection()");
        int Result = Client.GetProtection(Protection);
        if (Result==0)
        {
            System.out.println("sch_schal : "+Protection.sch_schal);
            System.out.println("sch_par   : "+Protection.sch_par);
            System.out.println("sch_rel   : "+Protection.sch_rel);
            System.out.println("bart_sch  : "+Protection.bart_sch);
            System.out.println("anl_sch   : "+Protection.anl_sch);
        }
        TestEnd(Result);
    }
        
    public static void Summary()
    {
    	System.out.println();
        System.out.println("+================================================================");
    	System.out.println("Tests performed : "+(ok+ko));
    	System.out.println("Passed          : "+ok);
    	System.out.println("Failed          : "+ko);    	
        System.out.println("+================================================================");
    }
    
    public static boolean Connect()
    {
    	TestBegin("ConnectTo()");
    	Client.SetConnectionType(S7.OP);
    	int Result = Client.ConnectTo(IpAddress, Rack, Slot);
    	if (Result==0)
    	{
            System.out.println("Connected to   : " + IpAddress + " (Rack=" + Rack + ", Slot=" + Slot+ ")");
            System.out.println("PDU negotiated : " + Client.PDULength()+" bytes");
    	}
    	TestEnd(Result);
    	return Result == 0;
    }
    
    public static void PerformTests()
    {
        GetSysInfo();
        GetProtectionScheme();
        GetDateAndTime();
        if (MakeAllTests)
        	SyncDateAndTime();
        ReadSzl();
        ShowStatus();
        if (MakeAllTests)
        	RunStop();
        BlockInfo(S7.Block_SFC,1); // Get SFC 1 info (always present in a CPU)
        DBPlay();
        Summary();
    }
    
    public static void Usage()
    {
        System.out.println("Usage");
        System.out.println("  client <IP> [Rack=0 Slot=2]");
        System.out.println("Example");
        System.out.println("  client 192.168.1.101 0 2");
        System.out.println("or");
        System.out.println("  client 192.168.1.101");    	
    }
    
    public static void schedulev2(String ipAddress,int equalization,Machine machine, int machineId) {
		//available bytes of data from plc driver
		byte[] data = new byte[300];
		
		//creating new S7Client
		S7Client client = new S7Client();
		
		//choosing connection type
		client.SetConnectionType (S7.S7_BASIC);
		
		S7 dataArea = new S7();
    	
    	boolean status=true;
    	while(status) {
    		
    		//first 15 bytes are reserved for sheet counter from machine
    		int res = client.ConnectTo(ipAddress, 0, 0);
    		int result = client.ReadArea(S7.S7AreaDB,1,0,15,data); 
    		
    		client.Disconnect();
    		
    		//equalization is a number of sheets produced before installation plc driver
    		int resultMachineId=Integer.sum(dataArea.GetDIntAt(data,0), equalization);
    		
    		try {
    			machine.setCounter(resultMachineId);
    			machineService.updateMachine(machineId, machine);
    		} catch(Exception e) {
    			e.printStackTrace();
    		}
    	}
	} 	
}
