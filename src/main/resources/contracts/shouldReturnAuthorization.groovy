import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method POST()
        url "/authenticate"
        headers {
            contentType applicationJson()
        }
        
        body('''
        	{
    			"username":"user",
    			"password":"$2y$12$RLizx4CyzeE0Tp4r0I9rBurYkJJfoOm9iJqCrKaQW/W5XJQjt8msC",
    			"role":"ROLE_USER"
			}'''
        )

    }
    
    response {
        status OK()
    }


}
