DROP TABLE IF EXISTS machine;

CREATE TABLE machine (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(120) NOT NULL,
    counter LONG NOT NULL
);

INSERT INTO machine (name, counter) values
('machine1',0),
('machine2',0),
('machine3',0);
