package pl.sebhaw.counters.controller;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.sebhaw.counters.entity.Machine;
import pl.sebhaw.counters.jwt.JwtTokenUtil;
import pl.sebhaw.counters.jwt.JwtUserDetailsService;
import pl.sebhaw.counters.service.MachineServiceImpl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Random;

import javax.transaction.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class MachineControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	private String userToken;
	
	private String adminToken;
	
	private ObjectMapper mapper;
	
	@Mock
	private MachineServiceImpl machineService;
	
	@InjectMocks
	private MachineController machineController;
	
	@BeforeEach
	public void authorizationWithUserRole() {
		
		final UserDetails userDetails = userDetailsService.loadUserByUsername("user");
		userToken = "Bearer "+jwtTokenUtil.generateToken(userDetails);
		mapper = new ObjectMapper();
	}
	
	@BeforeEach
	public void authorizationWithAdminRole() {
		final UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
		adminToken = "Bearer "+jwtTokenUtil.generateToken(userDetails);
		mapper = new ObjectMapper();
	}
	
	@Test
	@DisplayName("Test GET '/machine' with user creditials")
	@Rollback(true)
	public void shouldReturnHttpStatusIsOkAndListOfMachinesByUser() throws Exception {
		
		ResultActions result = mockMvc.perform(get("/machine")
									  .header("Authorization",userToken)
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isOk())
									  .andDo(MockMvcResultHandlers.print());
		
		MvcResult mvcResult = result.andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		Machine[] machinesValue = mapper.readValue(content, Machine[].class);
		
		for(Machine machine : machinesValue) {
			assertAll(
					() -> assertNotNull(machine.getId()),
					() -> assertNotNull(machine.getName()),
					() -> assertNotNull(machine.getCounter())
				);
		}
	}
	
	@Test
	@DisplayName("Test GET '/machine' with invalid jwt token")
	@Rollback(true)
	public void shouldReturnHttpStatusIsUnauthorized() throws Exception {
		
		ResultActions result = mockMvc.perform(get("/machine")
									  .header("Authorization", "random invalid jwt token")
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isUnauthorized())
									  .andDo(MockMvcResultHandlers.print());
		
		MvcResult mvcResult = result.andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		
		assertThat(content, CoreMatchers.equalTo(""));
	}
	
	@Test
	@DisplayName("Test GET '/machine' with admin creditials")
	@Rollback(true)
	public void shouldReturnStatusIsOkAndListOfMachinesByAdmin() throws Exception {
		
		ResultActions result = mockMvc.perform(get("/machine")
									  .header("Authorization",adminToken)
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isOk())
									  .andDo(MockMvcResultHandlers.print());
				
				MvcResult mvcResult = result.andReturn();
				String content = mvcResult.getResponse().getContentAsString();
				Machine[] machinesValue = mapper.readValue(content, Machine[].class);
				
				for(Machine machine : machinesValue) {
					assertAll(	
							() -> assertNotNull(machine.getId()),
							() -> assertNotNull(machine.getName()),
							() -> assertNotNull(machine.getCounter())
						);
				}
	}
	
	@Test
	@DisplayName("Test GET '/machine/{id} with user creditials")
	@Rollback(true)
	public void shouldReturnStatusIsOkAndListOfOneMachineByUser() throws Exception {
		
		ResultActions result = mockMvc.perform(get("/machine/1")
									  .header("Authorization",userToken)
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isOk())
									  .andDo(MockMvcResultHandlers.print());
				
				MvcResult mvcResult = result.andReturn();
				String content = mvcResult.getResponse().getContentAsString();
				Machine[] machinesValue = mapper.readValue(content, Machine[].class);
				
				for(Machine machine : machinesValue) {
					assertAll(
							() -> assertNotNull(machine.getId()),
							() -> assertNotNull(machine.getName()),
							() -> assertNotNull(machine.getCounter())
						);
				}
	}
	
	@Test
	@DisplayName("Test GET '/machine/{id} with admin creditials")
	@Rollback(true)
	public void shouldReturnStatusIsOkAndListOfOneMachineByAdmin() throws Exception {
		
		ResultActions result = mockMvc.perform(get("/machine/1")
									  .header("Authorization",adminToken)
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isOk())
									  .andDo(MockMvcResultHandlers.print());
				
				MvcResult mvcResult = result.andReturn();
				String content = mvcResult.getResponse().getContentAsString();
				Machine[] machinesValue = mapper.readValue(content, Machine[].class);
				
				for(Machine machine : machinesValue) {
					assertAll(
							() -> assertNotNull(machine.getId()),
							() -> assertNotNull(machine.getName()),
							() -> assertNotNull(machine.getCounter())
						);
				}
	}
	
	@Test
	@DisplayName("Test GET '/machine/{id}' with invalid jwt token")
	@Rollback(true)
	public void shouldReturnHttpStatusIsUnauthorizedWithId() throws Exception {
		
		ResultActions result = mockMvc.perform(get("/machine/1")
									  .header("Authorization", "random invalid jwt token")
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isUnauthorized())
									  .andDo(MockMvcResultHandlers.print());
		
		MvcResult mvcResult = result.andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		
		assertThat(content, CoreMatchers.equalTo(""));
	}
	
	@Test
	@DisplayName("Test DELETE '/machine/{id} with admin creditials with correct id")
	@Rollback(true)
	public void shouldReturnHttpStatusIsOkWhenDeleteMachineWithCorrectIdByAdmin() throws Exception {
		
		Mockito.doNothing().when(machineService).deleteMachine(ArgumentMatchers.anyInt());
		ResultActions result = mockMvc.perform(delete("/machine/1")
									  .header("Authorization",adminToken)
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isOk())
									  .andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	@DisplayName("Test DELETE '/machine/{id} with admin creditials with incorrect id")
	@Rollback(true)
	public void shouldReturnHttpStatusIsNotFoundMachineWithIncorrectIdByAdmin() throws Exception {
		
		Mockito.doNothing().when(machineService).deleteMachine(ArgumentMatchers.anyInt());
		ResultActions result = mockMvc.perform(delete("/machine/"+new Random().nextInt(99999-999)+999)
									  .header("Authorization", adminToken)
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isNotFound())
									  .andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	@DisplayName("Test DELETE '/machine/{id} with user creditials with incorrect id")
	@Rollback(true)
	public void shouldReturnHttpStatusIsUnauthorizedWithIncorrectIdByUser() throws Exception {
		
		Mockito.doNothing().when(machineService).deleteMachine(ArgumentMatchers.anyInt());
		ResultActions result = mockMvc.perform(delete("/machine/"+new Random().nextInt(99999-999)+999)
									  .header("Authorization", userToken)
									  .contentType(MediaType.APPLICATION_JSON))
									  .andExpect(status().isUnauthorized())
									  .andDo(MockMvcResultHandlers.print());
	}
	
}
