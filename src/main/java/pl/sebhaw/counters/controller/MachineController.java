package pl.sebhaw.counters.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sebhaw.counters.entity.Machine;
import pl.sebhaw.counters.service.IReturnPrincipal;
import pl.sebhaw.counters.service.MachineService;

@RestController
@Slf4j
@CrossOrigin
public class MachineController implements IReturnPrincipal{

    private final MachineService machineService;

    public MachineController(MachineService machineService) {
        this.machineService = machineService;
    }

    @GetMapping("/machine")
    public ResponseEntity getAllMachines() {
    	log.info("User: "+getPrincipal()+" requesting GetMapping: /machine with HttpStatus: "+HttpStatus.OK);
        return ResponseEntity.ok().body(machineService.getAllMachines());
    }

    @GetMapping("/machine/{id}")
    public ResponseEntity getMachine(@PathVariable int id) {
    	if(machineService.getMachine(id).isEmpty()) {
    		log.info("User: "+getPrincipal()+" requesting GetMapping: /machine/{id} with HttpStatus: "+HttpStatus.NOT_FOUND);
    		return ResponseEntity.notFound().build();
    	} else {
    		log.info("User: "+getPrincipal()+" requesting GetMapping: /machine/{id} with HttpStatus: "+HttpStatus.OK);
    		return ResponseEntity.ok().body(machineService.getMachine(id));
    	}
    }

    @PutMapping("/machine/{id}")
    public ResponseEntity updateMachine(@PathVariable int id, @RequestBody Machine machine) {
    	if(machineService.getMachine(id).isEmpty()) {
    		log.info("User: "+getPrincipal()+" requesting PutMapping: /machine/{id} with HttpStatus: "+HttpStatus.NOT_FOUND);
            return ResponseEntity.notFound().build();
    	}

    	machine.setId(id);
    	machineService.updateMachine(id, machine);
    	log.info("User: "+getPrincipal()+" requesting PutMapping: /machine/{id} with HttpStatus: "+HttpStatus.OK);
    	return ResponseEntity.ok().build();
    }

    @DeleteMapping("/machine/{id}")
    public ResponseEntity deleteMachine(@PathVariable int id) {
    	if(machineService.getMachine(id).isEmpty()) {
    		log.info("User: "+getPrincipal()+" requesting DeleteMapping: /machine/{id} with HttpStatus: "+HttpStatus.NOT_FOUND);
            return ResponseEntity.notFound().build();
    	} 
        machineService.deleteMachine(id);
        log.info("User: "+getPrincipal()+" requesting DeleteMapping: /machine/{id} with HttpStatus: "+HttpStatus.OK);
        return ResponseEntity.ok().build();
    }
}