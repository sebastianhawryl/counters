package pl.sebhaw.counters.service;

import pl.sebhaw.counters.entity.Machine;

import java.util.List;

public interface MachineService {
    List<Machine> getAllMachines();
    List<Machine> getMachine(int id);
    void addMachine(Machine machine);
    void updateMachine(int id, Machine machine);
    void deleteMachine(int id);
}
