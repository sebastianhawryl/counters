package pl.sebhaw.counters.architecture;

import org.junit.jupiter.api.Test;

import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;

public class PackageTest extends ArchitectureTest {

	@Test
	public void jwtClassesShouldOnlyBeAccessedByOtherJwtClasses() {
		ArchRule rule = ArchRuleDefinition.noClasses()
				.that().resideInAPackage(JWT_LAYER_PACKAGES)
			    .should().onlyBeAccessed().byAnyPackage(JWT_LAYER_PACKAGES);
			    rule.check(classes);
	}
	
	@Test
	public void RepositoryClassesShouldOnlyBeAccessedByOtherRepositoryClasses() {
		ArchRule rule = ArchRuleDefinition.noClasses()
				.that().resideInAPackage(REPOSITORY_LAYER_PACKAGES)
			    .should().onlyBeAccessed().byAnyPackage(REPOSITORY_LAYER_PACKAGES);
			    rule.check(classes);
	}
	
	@Test
	public void ServiceClassesShouldOnlyBeAccessedByOtherServiceClassesOrMoka7ClientClassesOrControllerClasses() {
		ArchRule rule = ArchRuleDefinition.noClasses()
				.that().resideInAPackage(SERVICE_LAYER_PACKAGES)
			    .should().onlyBeAccessed().byAnyPackage(SERVICE_LAYER_PACKAGES, MOKA7CLIENT_LAYER_PACKAGES, CONTROLLER_LAYER_PACKAGES);
			    rule.check(classes);
	}

	@Test
	public void Moka7ClassesShouldOnlyBeAccessedByOtherMoka7Classes() {
		ArchRule rule = ArchRuleDefinition.noClasses()
				.that().resideInAPackage(MOKA7_LAYER_PACKAGES)
			    .should().onlyBeAccessed().byAnyPackage(MOKA7_LAYER_PACKAGES);
			    rule.check(classes);
	}
}
