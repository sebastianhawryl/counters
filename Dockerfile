FROM openjdk:13-jdk-alpine
ADD target/counters-0.0.1-SNAPSHOT.jar .
EXPOSE 8000
CMD java -jar counters-0.0.1-SNAPSHOT.jar